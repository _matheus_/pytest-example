
# 1 = pedra
# 2 = papel
# 3 = tesoura

class Player:
    def __init__(self, name) -> None:
        self.name = name
        self._wins = 0
        self._defeats = 0
        self._current_choice = None
        self._ties = 0

    def select_choice(self, value):
        self._current_choice = value

    def clear_choice(self):
        self._current_choice = None

    @property
    def current_choice(self):
        return self._current_choice

    def increment_wins(self):
        self._wins += 1

    def increment_defeats(self):
        self._defeats += 1

    def increment_ties(self):
        self._ties += 1

    @property
    def wins(self):
        return self._wins

    @property
    def defeats(self):
        return self._defeats

    @property
    def ties(self):
        return self._ties


class JokenpoGame:
    def __init__(self) -> None:
        self.rules = [
            (1, 3),  # pedra -> tesoura
            (2, 1),  # tesoura -> papel
            (3, 2)  # papel -> pedra
        ]

        self.player1 = None
        self.player2 = None

    def start(self, player1: Player, player2: Player):
        self.player1 = player1
        self.player2 = player2

    def _validate_players(self):
        if self.player1 == None:
            raise PlayerNotFound('Player 1 is not set')
        if self.player2 == None:
            raise PlayerNotFound('Player 2 is not set')

    def _validate_players_choice(self):
        valid_choices = (1, 2, 3)

        if self.player1.current_choice not in valid_choices:
            raise InvalidChoiceException('Invalid choice of player 1')

        if self.player2.current_choice not in valid_choices:
            raise InvalidChoiceException('Invalid choice of player 2')

    def compute_choices(self):
        self._validate_players()
        self._validate_players_choice()

        for r in self.rules:
            if self.player1.current_choice in r and self.player2.current_choice in r:
                won, _ = r
                if self.player1.current_choice == self.player2.current_choice:
                    self.player1.increment_ties()
                    self.player2.increment_ties()
                elif self.player1.current_choice == won:
                    self.player1.increment_wins()
                    self.player2.increment_defeats()
                else:
                    self.player2.increment_wins()
                    self.player1.increment_defeats()
                self.player1.clear_choice()
                self.player2.clear_choice()

    def print_score(self):
        print('=====SCORE=====')
        print('## Player 1')
        print(f'Wins: {self.player1.wins}')
        print(f'Defeats: {self.player1.defeats}')
        print('\n== X ==\n')
        print('## Player 2')
        print(f'Wins: {self.player2.wins}')
        print(f'Defeats: {self.player2.defeats}')
        print('================')


class PlayerNotFound(Exception):
    pass


class InvalidChoiceException(Exception):
    pass


if __name__ == '__main__':
    p1 = Player('Matheus')
    p2 = Player('Ana')
    game = JokenpoGame()
    game.start(p1, p2)

    p1.select_choice(1)
    p2.select_choice(2)
    game.compute_choices()
    game.print_score()

    p1.select_choice(3)
    p2.select_choice(5)
    game.compute_choices()
    game.print_score()
