def avaliar_vetor(vetor: list):
    vetor_pares = []
    vetor_impares = []

    for v in vetor:
        if v % 2 == 0:
            vetor_pares.append(v)
        else:
            vetor_impares.append(v)

    return (vetor_pares, vetor_impares)


if __name__ == '__main__':
    vetor = []

    vetor.append(input(''))

    resultado = avaliar_vetor(vetor)
    print(resultado)
