import pytest
from src.calculadora import soma, divisao


def test_soma():
    assert soma(2, 2) == 4


def test_divisao():
    with pytest.raises(ZeroDivisionError) as e:
        divisao(2, 0) == 1
