import pytest
from src.game import Player, JokenpoGame, InvalidChoiceException, PlayerNotFound


@pytest.mark.parametrize('input, result', [
    ((3, 1), (0, 1)),
    ((1, 2), (0, 1))
])
def test_deve_fazer_uma_jogada_e_player2_deve_ganhar(input, result):
    p1 = Player('fulano')
    p2 = Player('siclano')

    game = JokenpoGame()
    game.start(p1, p2)

    p1.select_choice(input[0])
    p2.select_choice(input[1])
    game.compute_choices()

    assert p1.wins == result[0]
    assert p2.wins == result[1]


def test_player1_deve_informar_opcao_invalida_e_excessao_deve_ser_lancada():
    p1 = Player('fulano')
    p2 = Player('siclano')

    game = JokenpoGame()
    game.start(p1, p2)

    p1.select_choice(1)
    p2.select_choice(4)

    with pytest.raises(InvalidChoiceException) as e:
        game.compute_choices()


def test_deve_rodar_o_jogo_sem_os_players_definidos_e_excessao_deve_ser_lancada():
    p1 = Player('fulano')
    p2 = Player('siclano')

    game = JokenpoGame()
    # game.start(p1, p2)

    p1.select_choice(1)
    p2.select_choice(4)

    with pytest.raises(PlayerNotFound) as e:
        game.compute_choices()
