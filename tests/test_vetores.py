import pytest
from src.vetores import avaliar_vetor


@pytest.mark.parametrize('input, even_result, odd_result', [
    ([1, 2, 3, 4, 5, 6], [2, 4, 6], [1, 3, 5])
])
def test_vetores(input, even_result, odd_result):
    pares, impares = avaliar_vetor(input)
    assert pares == even_result
    assert impares == odd_result
